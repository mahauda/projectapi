package universite.angers.master.info.api.translater;

/**
 * Interface qui permet de traduire un message T vers K et inversement
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Translable<T, K> {

	/**
	 * Traduire un message K en un message T
	 * @param message K à traduire
	 * @return le message traduit dans la langue T
	 */
	public T translateMessageKToT(K message);
	
	/**
	 * Traduire un message T en un message K
	 * @param message T à traduire
	 * @return le message traduit dans la langue K
	 */
	public K translateMessageTToK(T message);
}
