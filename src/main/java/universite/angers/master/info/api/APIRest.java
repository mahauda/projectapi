package universite.angers.master.info.api;

import org.apache.log4j.Logger;

/**
 * Classe qui permet de rendre interopérable une application
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public abstract class APIRest<T, K> {
	
	/**
	 * Log de classe APIRest
	 */
	private static final Logger LOG = Logger.getLogger(APIRest.class);
	
	/**
	 * Interface qui permet de communiquer avec une structure de données. Base de données, fichier, etc...
	 */
	protected Accessiable<T, K> data;
	
	/**
	 * L'encodage du message
	 */
	protected Charset characterEncoding;
	
	/**
	 * Le contenu type du message (XML, JSON, ...)
	 */
	protected MimeType contentType;
	
	/**
	 * Le résultat de la réponse
	 */
	protected HttpStatusCode result;
	
	public APIRest(Charset characterEncoding, MimeType contentType,
			Accessiable<T, K> data) {
		this.characterEncoding = characterEncoding;
		LOG.debug("Charset : " + this.characterEncoding);
		
		this.contentType = contentType;
		LOG.debug("MimeType : " + this.contentType);
		
		this.data = data;
		LOG.debug("Data : " + this.data);
	}

	/**
	 * @return the data
	 */
	public Accessiable<T, K> getData() {
		return data;
	}

	/**
	 * @param data the data to set
	 */
	public void setData(Accessiable<T, K> data) {
		this.data = data;
	}

	/**
	 * @return the characterEncoding
	 */
	public Charset getCharacterEncoding() {
		return characterEncoding;
	}

	/**
	 * @param characterEncoding the characterEncoding to set
	 */
	public void setCharacterEncoding(Charset characterEncoding) {
		this.characterEncoding = characterEncoding;
	}

	/**
	 * @return the contentType
	 */
	public MimeType getContentType() {
		return contentType;
	}

	/**
	 * @param contentType the contentType to set
	 */
	public void setContentType(MimeType contentType) {
		this.contentType = contentType;
	}

	/**
	 * @return the result
	 */
	public HttpStatusCode getResult() {
		return result;
	}

	/**
	 * @param result the result to set
	 */
	public void setResult(HttpStatusCode result) {
		this.result = result;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((characterEncoding == null) ? 0 : characterEncoding.hashCode());
		result = prime * result + ((contentType == null) ? 0 : contentType.hashCode());
		result = prime * result + ((data == null) ? 0 : data.hashCode());
		result = prime * result + ((this.result == null) ? 0 : this.result.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		APIRest<?, ?> other = (APIRest<?, ?>) obj;
		if (characterEncoding != other.characterEncoding)
			return false;
		if (contentType != other.contentType)
			return false;
		if (data == null) {
			if (other.data != null)
				return false;
		} else if (!data.equals(other.data))
			return false;
		if (result != other.result)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "APIRest [data=" + data + ", characterEncoding=" + characterEncoding + ", contentType=" + contentType
				+ ", result=" + result + "]";
	}
}
