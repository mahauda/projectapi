package universite.angers.master.info.api;

import java.util.Collection;
import org.apache.log4j.Logger;
import com.google.gson.reflect.TypeToken;
import universite.angers.master.info.api.translater.json.TranslaterCollectionObjectToJson;
import universite.angers.master.info.api.translater.json.TranslaterJsonFactory;
import universite.angers.master.info.api.translater.json.TranslaterJsonToCollectionObject;
import universite.angers.master.info.api.translater.json.TranslaterJsonToObject;
import universite.angers.master.info.api.translater.json.TranslaterObjectToJson;

/**
 * Classe qui permet de construire une API Rest JSON
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class APIRestFactory {

	private static final Logger LOG = Logger.getLogger(APIRestFactory.class);
	
	private APIRestFactory() {
		
	}
	
	/**
	 * API qui permet de convertir des objets en JSON
	 * @param <T>
	 * @param charset
	 * @param data
	 * @param typeTokenObject
	 * @param typeTokenCollectionObject
	 * @return
	 */
	public static <T> APIRestStringToObject<T> getAPIRestObjectToJson(Charset charset, Accessiable<T, Collection<T>> data, TypeToken<T> typeTokenObject, TypeToken<Collection<T>> typeTokenCollectionObject) {
		TranslaterObjectToJson<T> translaterObjectToJson = TranslaterJsonFactory.getTranslaterObjectToJson(typeTokenObject);
		TranslaterCollectionObjectToJson<T> translaterCollectionObjectToJson = TranslaterJsonFactory.getTranslaterCollectionObjectToJson(typeTokenCollectionObject);
		
		return new APIRestStringToObject<>(charset, MimeType.JSON, data, translaterObjectToJson, translaterCollectionObjectToJson);
	}
	
	/**
	 * API qui permet de convertir du json en des objets
	 * @param <T>
	 * @param charset
	 * @param data
	 * @param typeTokenObject
	 * @param typeTokenCollectionObject
	 * @return
	 */
	public static <T> APIRestObjectToString<T> getAPIRestJsonToObject(Charset charset, Accessiable<String, String> data, TypeToken<T> typeTokenObject, TypeToken<Collection<T>> typeTokenCollectionObject) {
		TranslaterJsonToObject<T> translaterJsonToObject = TranslaterJsonFactory.getTranslaterJsonToObject(typeTokenObject);
		TranslaterJsonToCollectionObject<T> translaterJsonToCollectionObject = TranslaterJsonFactory.getTranslaterJsonToCollectionObject(typeTokenCollectionObject);
		
		return new APIRestObjectToString<>(charset, MimeType.JSON, data, translaterJsonToObject, translaterJsonToCollectionObject);
	}
}
