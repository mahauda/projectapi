package universite.angers.master.info.api;

import java.util.Collection;
import java.util.Map;
import org.apache.log4j.Logger;
import universite.angers.master.info.api.translater.Translable;

/**
 * Classe qui permet de rendre interopérable une application
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class APIRestStringToObject<T> extends APIRest<T, Collection<T>> implements Interoperable<String, String> {
	
	/**
	 * Log de classe APIRest
	 */
	private static final Logger LOG = Logger.getLogger(APIRestStringToObject.class);
	
	/**
	 * Interface qui permet de traduire une donnée vers une autre et inversement
	 */
	protected Translable<T, String> translaterObject;
	
	/**
	 * Interface qui permet de traduire une collection de données vers une autre et inversement
	 */
	protected Translable<Collection<T>, String> translaterCollectionObject;
	
	public APIRestStringToObject(Charset characterEncoding, MimeType contentType,
			Accessiable<T, Collection<T>> data, Translable<T, String> translaterObject, 
			Translable<Collection<T>, String> translaterCollectionObject) {
		super(characterEncoding, contentType, data);

		this.translaterObject = translaterObject;
		LOG.debug("ConverterObject : " + this.translaterObject);
		
		this.translaterCollectionObject = translaterCollectionObject;
		LOG.debug("ConverterCollectionObject : " + this.translaterCollectionObject);
	}
	
	@Override
	public synchronized String create(String entity) {
		LOG.debug("String object : " + entity);
		
		if(entity == null) {
			this.result = HttpStatusCode.NO_CONTENT;
			return "";
		}
		
		T object = this.translaterObject.translateMessageKToT(entity);
		LOG.debug("Object : " + object);
		
		if(object == null) {
			this.result = HttpStatusCode.BAD_REQUEST;
			return "";
		}
		
		boolean create = this.data.create(object);
		LOG.debug("Create object : " + create);
		
		if(create) {
			this.result = HttpStatusCode.OK;
		} else {
			this.result = HttpStatusCode.NOT_FOUND;
		}
		
		return entity;
	}

	@Override
	public synchronized String update(String entity) {
		LOG.debug("String object : " + entity);
		
		if(entity == null) {
			this.result = HttpStatusCode.NO_CONTENT;
			return "";
		}
		
		T object = this.translaterObject.translateMessageKToT(entity);
		LOG.debug("Object : " + object);
		
		if(object == null) {
			this.result = HttpStatusCode.BAD_REQUEST;
			return "";
		}
		
		boolean update = this.data.update(object);
		LOG.debug("Update object : " + update);
		
		if(update) {
			this.result = HttpStatusCode.OK;
		} else {
			this.result = HttpStatusCode.NOT_FOUND;
		}
		
		return entity;
	}

	@Override
	public synchronized String delete(String entity) {
		LOG.debug("String object : " + entity);
		
		if(entity == null) {
			this.result = HttpStatusCode.NO_CONTENT;
			return "";
		}
		
		T object = this.translaterObject.translateMessageKToT(entity);
		LOG.debug("Object : " + object);
		
		if(object == null) {
			this.result = HttpStatusCode.BAD_REQUEST;
			return "";
		}
		
		boolean delete = this.data.delete(object);
		LOG.debug("Delete object : " + delete);
		
		if(delete) {
			this.result = HttpStatusCode.OK;
		} else {
			this.result = HttpStatusCode.NOT_FOUND;
		}
		
		return entity;
	}
	
	@Override
	public synchronized String read(Map<String, Object> where) {
		LOG.debug("Where : " + where);
		
		T object = this.data.read(where);
		
		LOG.debug("Read object : " + object);
		
		if(object == null) {
			this.result = HttpStatusCode.NOT_FOUND;
			return null;
		}
		
		this.result = HttpStatusCode.OK;
		return this.translaterObject.translateMessageTToK(object);
	}
	
	@Override
	public synchronized String read(String column, Object value) {
		LOG.debug("Column : " + column);
		LOG.debug("Value : " + value);
		
		T object = this.data.read(column, value);
		
		LOG.debug("Read object : " + object);
		
		if(object == null) {
			this.result = HttpStatusCode.NOT_FOUND;
			return null;
		}
		
		this.result = HttpStatusCode.OK;
		return this.translaterObject.translateMessageTToK(object);
	}
	
	@Override
	public synchronized String readAll() {
		Collection<T> objects = this.data.readAll();
		LOG.debug("Read objects : " + objects);
		
		if(objects == null) {
			this.result = HttpStatusCode.NO_CONTENT;
			return null;
		}
		
		this.result = HttpStatusCode.OK;
		return this.translaterCollectionObject.translateMessageTToK(objects);
	}
	
	@Override
	public synchronized String readAll(String column, Object value) {
		LOG.debug("Column : " + column);
		LOG.debug("Value : " + value);
		
		Collection<T> objects = this.data.readAll(column, value);
		LOG.debug("Read objects : " + objects);
		
		if(objects == null) {
			this.result = HttpStatusCode.NO_CONTENT;
			return null;
		}
		
		//Si un seul résultat resort dans ce cas read
		if(objects.size() == 1) {
			return this.read(column, value);
		}
		
		this.result = HttpStatusCode.OK;
		return this.translaterCollectionObject.translateMessageTToK(objects);
	}
	
	@Override
	public synchronized String readAll(Map<String, Object> where) {
		Collection<T> objects = this.data.readAll(where);
		LOG.debug("Read objects : " + objects);
		
		if(objects == null) {
			this.result = HttpStatusCode.NO_CONTENT;
			return null;
		}
		
		//Si un seul résultat resort dans ce cas read
		if(objects.size() == 1) {
			return this.read(where);
		}
				
		this.result = HttpStatusCode.OK;
		return this.translaterCollectionObject.translateMessageTToK(objects);
	}

	/**
	 * @return the translaterObject
	 */
	public Translable<T, String> getTranslaterObject() {
		return translaterObject;
	}

	/**
	 * @param translaterObject the translaterObject to set
	 */
	public void setTranslaterObject(Translable<T, String> translaterObject) {
		this.translaterObject = translaterObject;
	}

	/**
	 * @return the translaterCollectionObject
	 */
	public Translable<Collection<T>, String> getTranslaterCollectionObject() {
		return translaterCollectionObject;
	}

	/**
	 * @param translaterCollectionObject the translaterCollectionObject to set
	 */
	public void setTranslaterCollectionObject(Translable<Collection<T>, String> translaterCollectionObject) {
		this.translaterCollectionObject = translaterCollectionObject;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((translaterCollectionObject == null) ? 0 : translaterCollectionObject.hashCode());
		result = prime * result + ((translaterObject == null) ? 0 : translaterObject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		APIRestStringToObject<?> other = (APIRestStringToObject<?>) obj;
		if (translaterCollectionObject == null) {
			if (other.translaterCollectionObject != null)
				return false;
		} else if (!translaterCollectionObject.equals(other.translaterCollectionObject))
			return false;
		if (translaterObject == null) {
			if (other.translaterObject != null)
				return false;
		} else if (!translaterObject.equals(other.translaterObject))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "APIRestStringToObject [translaterObject=" + translaterObject + ", translaterCollectionObject="
				+ translaterCollectionObject + "]";
	}
}
