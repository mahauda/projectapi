package universite.angers.master.info.api.converter;

/**
 * Interface qui permet de convertir un message K vers un message T
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Convertable<T, K> {
	
	/**
	 * Convertir un message K vers un message T
	 * @param message
	 * @return
	 */
	public T convert(K message);
}
