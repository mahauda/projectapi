package universite.angers.master.info.api;

import java.util.Collection;
import java.util.Collections;
import java.util.Map;
import org.apache.log4j.Logger;

import universite.angers.master.info.api.translater.Translable;

/**
 * Classe qui permet de rendre interopérable une application
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class APIRestObjectToString<T> extends APIRest<String, String> implements Interoperable<T, Collection<T>> {
	
	/**
	 * Log de classe APIRest
	 */
	private static final Logger LOG = Logger.getLogger(APIRestObjectToString.class);
	
	/**
	 * Interface qui permet de traduire une donnée vers une autre et inversement
	 */
	protected Translable<String, T> translaterObject;
	
	/**
	 * Interface qui permet de traduire une collection de données vers une autre et inversement
	 */
	protected Translable<String, Collection<T>> translaterCollectionObject;
	
	public APIRestObjectToString(Charset characterEncoding, MimeType contentType,
			Accessiable<String, String> data, Translable<String, T> translaterObject, 
			Translable<String, Collection<T>> translaterCollectionObject) {
		super(characterEncoding, contentType, data);
		
		this.translaterObject = translaterObject;
		LOG.debug("ConverterObject : " + this.translaterObject);
		
		this.translaterCollectionObject = translaterCollectionObject;
		LOG.debug("ConverterCollectionObject : " + this.translaterCollectionObject);
	}
	
	@Override
	public synchronized T create(T entity) {
		LOG.debug("String object : " + entity);
		
		if(entity == null) {
			this.result = HttpStatusCode.NO_CONTENT;
			return null;
		}
		
		String object = this.translaterObject.translateMessageKToT(entity);
		LOG.debug("Object : " + object);
		
		if(object == null) {
			this.result = HttpStatusCode.BAD_REQUEST;
			return null;
		}
		
		boolean create = this.data.create(object);
		LOG.debug("Create object : " + create);
		
		if(create) {
			this.result = HttpStatusCode.OK;
		} else {
			this.result = HttpStatusCode.NOT_FOUND;
		}
		
		return entity;
	}

	@Override
	public synchronized T update(T entity) {
		LOG.debug("String object : " + entity);
		
		if(entity == null) {
			this.result = HttpStatusCode.NO_CONTENT;
			return null;
		}
		
		String object = this.translaterObject.translateMessageKToT(entity);
		LOG.debug("Object : " + object);
		
		if(object == null) {
			this.result = HttpStatusCode.BAD_REQUEST;
			return null;
		}
		
		boolean update = this.data.update(object);
		LOG.debug("Update object : " + update);
		
		if(update) {
			this.result = HttpStatusCode.OK;
		} else {
			this.result = HttpStatusCode.NOT_FOUND;
		}
		
		return entity;
	}

	@Override
	public synchronized T delete(T entity) {
		LOG.debug("String object : " + entity);
		
		if(entity == null) {
			this.result = HttpStatusCode.NO_CONTENT;
			return null;
		}
		
		String object = this.translaterObject.translateMessageKToT(entity);
		LOG.debug("Object : " + object);
		
		if(object == null) {
			this.result = HttpStatusCode.BAD_REQUEST;
			return null;
		}
		
		boolean delete = this.data.delete(object);
		LOG.debug("Delete object : " + delete);
		
		if(delete) {
			this.result = HttpStatusCode.OK;
		} else {
			this.result = HttpStatusCode.NOT_FOUND;
		}
		
		return entity;
	}
	
	@Override
	public synchronized T read(String column, Object value) {
		String object = this.data.read(column, value);
		
		LOG.debug("Read object by id : " + object);
		
		if(object == null) {
			this.result = HttpStatusCode.NOT_FOUND;
			return null;
		}
		
		this.result = HttpStatusCode.OK;
		return this.translaterObject.translateMessageTToK(object);
	}
	
	@Override
	public synchronized T read(Map<String, Object> where) {
		String object = this.data.read(where);
		
		LOG.debug("Read object by id : " + object);
		
		if(object == null) {
			this.result = HttpStatusCode.NOT_FOUND;
			return null;
		}
		
		this.result = HttpStatusCode.OK;
		return this.translaterObject.translateMessageTToK(object);
	}
	
	@Override
	public synchronized Collection<T> readAll() {
		String objects = this.data.readAll();
		LOG.debug("Read objects : " + objects);
		
		if(objects == null) {
			this.result = HttpStatusCode.NO_CONTENT;
			return Collections.emptyList();
		}
		
		this.result = HttpStatusCode.OK;
		return this.translaterCollectionObject.translateMessageTToK(objects);
	}
	
	@Override
	public synchronized Collection<T> readAll(String column, Object value) {
		String objects = this.data.readAll(column, value);
		LOG.debug("Read objects : " + objects);
		
		if(objects == null) {
			this.result = HttpStatusCode.NO_CONTENT;
			return Collections.emptyList();
		}
		
		this.result = HttpStatusCode.OK;
		return this.translaterCollectionObject.translateMessageTToK(objects);
	}
	
	@Override
	public synchronized Collection<T> readAll(Map<String, Object> where) {
		String objects = this.data.readAll(where);
		LOG.debug("Read objects : " + objects);
		
		if(objects == null) {
			this.result = HttpStatusCode.NO_CONTENT;
			return Collections.emptyList();
		}
		
		this.result = HttpStatusCode.OK;
		return this.translaterCollectionObject.translateMessageTToK(objects);
	}

	/**
	 * @return the translaterObject
	 */
	public Translable<String, T> getTranslaterObject() {
		return translaterObject;
	}

	/**
	 * @param translaterObject the translaterObject to set
	 */
	public void setTranslaterObject(Translable<String, T> translaterObject) {
		this.translaterObject = translaterObject;
	}

	/**
	 * @return the translaterCollectionObject
	 */
	public Translable<String, Collection<T>> getTranslaterCollectionObject() {
		return translaterCollectionObject;
	}

	/**
	 * @param translaterCollectionObject the translaterCollectionObject to set
	 */
	public void setTranslaterCollectionObject(Translable<String, Collection<T>> translaterCollectionObject) {
		this.translaterCollectionObject = translaterCollectionObject;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + ((translaterCollectionObject == null) ? 0 : translaterCollectionObject.hashCode());
		result = prime * result + ((translaterObject == null) ? 0 : translaterObject.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		APIRestObjectToString<?> other = (APIRestObjectToString<?>) obj;
		if (translaterCollectionObject == null) {
			if (other.translaterCollectionObject != null)
				return false;
		} else if (!translaterCollectionObject.equals(other.translaterCollectionObject))
			return false;
		if (translaterObject == null) {
			if (other.translaterObject != null)
				return false;
		} else if (!translaterObject.equals(other.translaterObject))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "APIRestObjectToString [translaterObject=" + translaterObject + ", translaterCollectionObject="
				+ translaterCollectionObject + "]";
	}
}
