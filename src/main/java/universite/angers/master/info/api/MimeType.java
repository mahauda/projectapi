package universite.angers.master.info.api;

/**
 * Enumération des mimes types HTTP
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public enum MimeType {
	
	JSON("application/json"),
	XML("application/xml");
	
	/**
	 * Nom du mime type
	 */
	private String name;
	
	private MimeType(String name) {
		this.name = name;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
}
