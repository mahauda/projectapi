package universite.angers.master.info.api.translater;

import universite.angers.master.info.api.converter.ConverterStringToString;

/**
 * Classe qui permet de traduire une string vers une string
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public class TranslaterStringToString extends Translater<String, String> {

	public TranslaterStringToString() {
		super(new ConverterStringToString(), new ConverterStringToString());
	}
}
