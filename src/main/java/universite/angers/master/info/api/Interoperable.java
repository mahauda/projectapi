package universite.angers.master.info.api;

import java.util.Map;

/**
 * Opérations interopérable de l'application
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Interoperable<T, K> {

	public T create(T entity);
	public T read(String column, Object value);
	public T read(Map<String, Object> where);
	public K readAll();
	public K readAll(String column, Object value);
	public K readAll(Map<String, Object> where);
	public T update(T object);
	public T delete(T object);
}
