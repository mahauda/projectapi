package universite.angers.master.info.api;

import java.util.Map;

/**
 * Interface qui permet de communiquer avec une structure de données accessible (BD, fichier, etc.)
 * à travers des opérations CRUD
 * 
 * @copyright : Master in computer science at the university of angers
 * @date 09/04/2020
 * @author Théo MAHAUDA, Anas TAGUENITI, Mohamed OUHIRRA
 * @version 1.0
 */
public interface Accessiable<T, K> {
	
	/**
	 * Enregistrer un objet
	 * @param object
	 * @return vrai si enregistré. Faux dans le cas contraire
	 */
	public boolean create(T object);
	
	/**
	 * Récupère un objet par un champ
	 * @param column
	 * @param value
	 * @return
	 */
	public T read(String column, Object value);
	
	/**
	 * Récupère un objet par une map de champ
	 * @param where
	 * @return
	 */
	public T read(Map<String, Object> where);
	
	/**
	 * Récupère une collection d'objets
	 * @return les objets
	 */
	public K readAll();
	
	/**
	 * Récupère une collection d'objets sous conditions
	 * @param column le champ à interroger
	 * @param value la condition
	 * @return les objets respectant les conditions du where
	 */
	public K readAll(String column, Object value);
	
	/**
	 * Récupère une collection d'objets sous conditions
	 * Key = nom de la colonne
	 * Value = la valeur à rechercher pour la colonne
	 * @param where
	 * @return les objets respectant les conditions du where
	 */
	public K readAll(Map<String, Object> where);
	
	/**
	 * Mise à jour d'un objet
	 * @param object
	 * @return vrai si maj. Faux dans le cas contraire
	 */
	public boolean update(T object);
	
	/**
	 * Suppression d'un objet
	 * @param object
	 * @return vrai si supprimé. Faux dans le cas contraire
	 */
	public boolean delete(T object);
}
