# Projet API

Réalisé par :
- Théo MAHAUDA
- Anas TAGUENITI
- Mohamed OUHIRRA

Date : 09/04/2020\
Organisation : Master Informatique M1 à l'université d'Angers\
Version : 1.0\
Référence projet GitLab : https://gitlab.com/mahauda/projectapi

## Objectif

Permettre de convertir des objets java en format texte et inversement grâce au JSON